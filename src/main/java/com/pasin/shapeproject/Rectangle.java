/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pasin.shapeproject;

/**
 *
 * @author Pla
 */
public class Rectangle {
    private double h,w;
    public Rectangle(double h ,double w){
        this.w=w;
        this.h=h;
    }

    
    public double calArea(){
        return h*w;
    }
    public double getH(){
        return h;
    }
    public double getW(){
        return w;
    }
    public void setH(double h){
        if(h<=0){
            System.out.println("Error: high must more than zero!!!");
            return;
        }this.h =h;
    }public void setW(double w) {
        if(w<=0){
            System.out.println("Error: width must more than zero!!!");
            return;
        }this.w =w;
    }
    
    public String toSting(){
        return "Area of rectangle1 W = "+this.getW()+" And H = "+this.getH()+" is  "+this.calArea();
    }
}
