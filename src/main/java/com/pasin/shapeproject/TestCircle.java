/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pasin.shapeproject;

/**
 *
 * @author Pla
 */
public class TestCircle {
    public static void main(String[] args) {
        Circle circle1 =new Circle(3);
        System.out.println("Area of circle1 r = "+ circle1.getR() +") is " + circle1.calArea());
        circle1.setR(2);
        System.out.println("Area of circle1 r = "+ circle1.getR() +") is " + circle1.calArea());
        circle1.setR(0);
        System.out.println("Area of circle1 r = "+ circle1.getR() +") is " + circle1.calArea());
        System.out.println(circle1.toString());
        System.out.println(circle1);
    }
}
