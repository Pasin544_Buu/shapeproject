/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pasin.shapeproject;

/**
 *
 * @author Pla
 */
public class TestTriangle {
    public static void main(String[] args) {
        Triangle triangle1 = new Triangle(3,10);
        System.out.println("Area of triangle w = "+triangle1.getW()+" And H = "+triangle1.getH()+" is "+triangle1.calArea());
        triangle1.setW(2);
        triangle1.setH(3);
        System.out.println("Area of triangle w = "+triangle1.getW()+" And H = "+triangle1.getH()+" is "+triangle1.calArea());
        triangle1.setH(0);
        System.out.println("Area of triangle w = "+triangle1.getW()+" And H = "+triangle1.getH()+" is "+triangle1.calArea());
        triangle1.setW(0);
        System.out.println("Area of triangle w = "+triangle1.getW()+" And H = "+triangle1.getH()+" is "+triangle1.calArea());
        System.out.println(triangle1.toSting());
    }
}
