/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pasin.shapeproject;

/**
 *
 * @author Pla
 */
public class Square {
    private double w;
    public Square(double w){
        this.w=w;
    }
    public double calArea(){
        return w*w;
    
    }public double getW(){
        return w;
    }public void setW(double w) {
        if(w<=0){
            System.out.println("Error: width must more than zero!!!");
            return;
        }this.w =w;
    }
    
}
