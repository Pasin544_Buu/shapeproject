/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pasin.shapeproject;

/**
 *
 * @author Pla
 */
public class TestRectangle {
    public static void main(String[] args) {
        Rectangle rectangle1 = new Rectangle(3,10);
        System.out.println("Area of rectangle1 W = "+rectangle1.getW()+" And H = "+rectangle1.getH()+" is  "+rectangle1.calArea());
        rectangle1.setH(10);
        rectangle1.setW(20);
        System.out.println("Area of rectangle1 W = "+rectangle1.getW()+" And H = "+rectangle1.getH()+" is  "+rectangle1.calArea());
        rectangle1.setH(0);
        rectangle1.setW(1);
        System.out.println("Area of rectangle1 W = "+rectangle1.getW()+" And H = "+rectangle1.getH()+" is  "+rectangle1.calArea());
        rectangle1.setH(1);
        rectangle1.setW(0);
        System.out.println("Area of rectangle1 W = "+rectangle1.getW()+" And H = "+rectangle1.getH()+" is  "+rectangle1.calArea());
        System.out.println(rectangle1.toSting());
        
    }
}
